# Relazione

> Vedi il file [00-consegna.pdf](00-consegna.pdf) più il [repository originale](https://gitlab.com/frfaenza/cloudedgecomputing/-/tree/01-flask-base-project) o [il primo commit di questo repository](https://gitlab.com/Steffo99/cinquantasei/-/tree/144586e35405941c66f2eadded47dbe7d902d8fb) per una descrizione della consegna di questo esame.

## Descrizione del progetto

Si è cercato di implementare progressivamente strumentazione di DevOps di vario tipo al progetto dato, prendendo in considerazione i livelli di:
- planning
- development
- deployment
- monitoring

Si è inoltre considerato il progetto come un piccolo sito web mantenuto da pochi sviluppatori, le cui priorità sono concentrarsi sulla scrittura di codice e non sulla manutenzione dell'infrastruttura necessaria a eseguirlo.

## Planning

### GitLab

Si sono abilitate le issues di GitLab nel pannello delle impostazioni:

![](01-issues.png)

Successivamente, è stata creata una issue per ciascun punto proposto in fase di personalizzazione dell'esame, imitando come verrebbe fatto con le feature da aggiungere all'applicazione in un vero progetto:

[→ Vai all'elenco di tutte le issues](https://gitlab.com/Steffo99/cinquantasei/-/issues/?sort=created_date&state=all&first_page_size=20)

![](01-issuelist.png)

Infine, a ciascuna issue sono state collegate le altre che la bloccavano, in modo da avere un'idea dell'ordine da seguire nel loro sviluppo:

[→ Vai a una issue di esempio](https://gitlab.com/Steffo99/cinquantasei/-/issues/7)

![](01-linked.png)

> Aggregando in un posto unico discussioni relative alle feature da implementare nel progetto, si permette ai manutentori di coordinarsi meglio e di ricordarsi i piani fatti anche molto tempo prima.

## Development

### Git

Si è abilitato il repository Git nel pannello delle impostazioni di GitLab:

![](02-repository.png)

Si sono copiati manualmente i file dal [repository originale](https://gitlab.com/frfaenza/cloudedgecomputing/-/tree/01-flask-base-project) a una nuova cartella nel computer locale, e poi si è creato un nuovo repository, inizializzato con un singolo commit accreditato all'autore originale del progetto.

```shell
git add .
git commit --author="Francesco Faenza <dev@francescofaenza.it>" --message="Exercise start"
```

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/144586e35405941c66f2eadded47dbe7d902d8fb)

> In questo modo, la storia del branch è pulita, ed è più facile per i maintainer del progetto vedere quali modifiche sono state apportate e perchè.

Si è inoltre modificato il file `.gitignore` 

→ Vedi commit associati: [(1)](https://gitlab.com/Steffo99/cinquantasei/-/commit/ae179d43755ee6aa9ecfca00b8ab9513074e751f) [(2)](https://gitlab.com/Steffo99/cinquantasei/-/commit/6094b4067d679c32f29bfc1f5eabb39f88147c7e) [(3)](https://gitlab.com/Steffo99/cinquantasei/-/commit/cf0fbd23d641f722ced8c758ac6b09b9302a8abe) [(4)](https://gitlab.com/Steffo99/cinquantasei/-/commit/0baffd85ddd84e6e4c8260fb53f057a2e3600b44)

### IntelliJ IDEA

Si è aperto il progetto con l'ambiente di sviluppo integrato [IntelliJ IDEA](https://www.jetbrains.com/idea/).

Si è rimossa l'esclusione `.idea` dal `.gitignore` del progetto, in quanto esso è già gestito dall'IDE.

Si è configurato nome, icona, e struttura directory del progetto:

![](02-ideamodules.png)

Si è inoltre creata una run configuration che avviasse o debuggasse il development server:

![](02-idearun.png)

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/b49f7682bb191188194febd6d6e8b79b451f4b63)

> In questo modo, i maintainer che desiderassero utilizzare quello specifico IDE si troveranno già il progetto configurato per lo sviluppo.

Si è poi eseguito dall'IDE un singolo passaggio di "Reformat code".

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/53c848fc5d6bbf5f9ed8f5d45815e9e933b19ed0)

> In questo modo, il codice segue una formattazione standardizzata, diventando più facile da leggere e da comprendere.

### Flask

Si è configurato il server di sviluppo per usare di default la porta 8080, [standardizzata dall'IANA come porta HTTP alternativa](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers) qualora non fosse disponibile la porta 80, normalmente riservata al sistema operativo e non utilizzabile in ambienti di sviluppo locali. 

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/75ed3a9b2c765a4e39557071ee7d38703bf8c778)

> In questo modo è immediatamente chiaro il protocollo utilizzato dal programma in sviluppo e si evitano rari ma possibili conflitti di porta.

### cfig

Si è sostituita la configurazione dell'applicazione hard-coded negli script Python con una variabile definita attraverso [cfig](https://cfig.readthedocs.io/en/latest/index.html), libreria per la configurazione interattiva di applicazioni Python.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/20d55cc5a36e994337260d4d53269d488f9e301f)

[→ Vedi altro commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/6b0396b91ecf8ace1f06858585ae997aab293b10)

> In questo modo:
> 
> - si permette la configurazione dell'applicazione tramite variabili d'ambiente
>   - necessaria per configurare l'applicazione containerizzata
> - si permette la configurazione dell'applicazione tramite file
>   - necessaria per eventualmente usare l'applicazione containerizzata con Docker Swarm
> - si rende possibile la visualizzazione human-friendly della configurazione attuale
>   - utile per identificare velocemente errori di configurazione
> - si rende possibile aggiungere nuove variabili configurabili in modo pulito
>   - utile per alcuni passi successivi, che ne introdurranno

Si è successivamente modificata la run configuration di IntelliJ IDEA creata in precedenza per utilizzare [un database SQLite in-memory](https://docs.sqlalchemy.org/en/20/core/engines.html#sqlite).

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/8e9963db16df6a234fd3095be34342b785607587)

> In questo modo i manutentori non hanno bisogno di configurare un database sul loro ambiente di sviluppo locale per eseguire all'applicazione.

Si è infine rimosso uno statement che configurava parti dell'applicazione in modo insolito, sostituendolo con una dichiarazione esplicita.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/3426a4c514ae35ae2db28da94001fbc8bb233102)

> In questo modo l'introduzione di nuove variabili nello scope non rischia di andare a interferire con la configurazione esistente.

### WSGI

Si è rimossa la main clause dal file `wsgi.py`.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/f30a627c175e023b3a3d698f4da0654a2296bea5)

> In questo modo, si è certi che il server non si avvierà se non è configurato un backend WSGI, rendendo più facile identificare errori di configurazione.

Si è poi introdotto il middleware WSGI [`ProxyFix`](https://werkzeug.palletsprojects.com/en/2.2.x/middleware/proxy_fix/).

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/51c481fd0267fbc4199240e95809308c1e6d0a61)

> In questo modo, si permette a Flask di processare correttamente informazioni sulla richiesta HTTP qualora l'applicazione si trovi dietro un reverse proxy, che verrà usato per mettere il sito web in produzione.

### Alembic

Si è rigenerata la directory `migrations` di Alembic, incorrettamente esclusa dal repository originale.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/be51810c419c148cf1e026d1258726a5f840f5d9)

Successivamente, si è creata una prima migrazione che crea/distrugge la tabella `post_views` definita nell'applicazione.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/88b785a177e97e2cd31a6ed5d28377fc9ae08660)

> In questo modo, sarà possibile alterare il database in modo retrocompatibile, permettendo l'introduzione futura di nuove tabelle, o il rollback del database nel caso un aggiornamento vada storto.

## Deployment

### Docker

Si è inizialmente creato un file `.dockerignore` derivato dal file `.gitignore`, che esclude tutti i file esterni all'applicazione stessa dal Docker build context.

Si è poi creato un `Dockerfile` [multi-stage](https://docs.docker.com/build/building/multi-stage/) per l'applicazione, con tre diversi build target:
- `dev_app`, che esegue l'app con il webserver Flask per lo sviluppo
- `gunicorn_app`, che esegue l'app con il backend WSGI Gunicorn
- `gunicorn_reverse_proxy_app`, che esegue l'app con il middleware WSGI ProxyFix precedentemente configurato e il backend WSGI Gunicorn

→ Vedi commit associati: [(1)](https://gitlab.com/Steffo99/cinquantasei/-/commit/5e87634f218340b9a04c13597b2b17f21d5cd8cc) [(2)](https://gitlab.com/Steffo99/cinquantasei/-/commit/18e25a86d207a177c3663f1e0823726d3c7d39a1) [(3)](https://gitlab.com/Steffo99/cinquantasei/-/commit/ce420ca782d3f8eb47cd51fea8d7aab4a1e3b5db) [(4)](https://gitlab.com/Steffo99/cinquantasei/-/commit/6cac95b592beefe7bdd02960f9803220a02f8a0a) [(5)](https://gitlab.com/Steffo99/cinquantasei/-/commit/6aee26ef99b2ddaf877adb37e5cd6dd0d6a8f5f9) [(6)](https://gitlab.com/Steffo99/cinquantasei/-/commit/790150eb3339a256e2033a08722e77aea750a8c6)

> In questo modo diventa possibile creare una container image per l'applicazione, isolandone le dipendenze e facilitandone l'installazione su qualsiasi sistema.

### Docker Compose

Si è creato un file `compose.yml` che dichiara l'architettura di servizi per mettere in produzione l'applicazione, che inizialmente include solo un database [PostgreSQL](https://www.postgresql.org/).

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/201cc32bf62d28e797762074a0f2f060c7253e46)

> In questo modo, l'applicazione può essere messa rapidamente in produzione su qualsiasi sistema.

### Caddy

Si è scelto di utilizzare [Caddy](https://caddyserver.com/) come reverse proxy per via della sua semplicità di configurazione e gestione, che lo rende particolarmente adeguato per minimizzare il lavoro richiesto dall'ipotetico piccolo team di manutenzione del sito.

Si è creato un `Caddyfile` per l'utilizzo dallo stack Docker Compose che effettua reverse proxying dell'applicazione, e si è aggiunto Caddy allo stack stesso, integrandolo così all'ambiente di produzione.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/3248fab7f937064f476d8f1977caa609e3a1fce2)

[→ Vedi altro commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/203bb6a6def3b04b66aefe597025119d31c6c598)

> In questo modo sarà possibile sfruttare alcune funzionalità di Caddy descritte più avanti.

### GitLab CI + Flake8

Si è definito un job GitLab che esegue ad ogni push o merge request il tool [flake8](https://flake8.pycqa.org/en/latest/) per il linting dei file Python del progetto.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/3db2a66c265725aa86ef2e9381cdea4ba4754b5f)

[→ Vedi altro commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/925f218aa9b1b8430766286661b5f35e5688bf2e)

> In questo modo sarà possibile per i maintainer identificare rapidamente i problemi più semplici del codice esistente e che sta per essere aggiunto.

### GitLab CI + ESLint

Allo stesso modo, si è definito un job GitLab che esegue ad ogni push o merge request il tool [ESLint](https://eslint.org/) per il linting dei file JavaScript del progetto.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/d78b8397dcc0a9d2882b4e389b51bc0bf13be46e)

### GitLab CI + Hadolint

Ancora, si è definito un job GitLab che esegue ad ogni push o merge request il tool [Hadolint](https://github.com/hadolint/hadolint) per il linting del Dockerfile del progetto.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/d78b8397dcc0a9d2882b4e389b51bc0bf13be46e)

### GitLab CI + markdownlint-cli2

Infine, si è definito un job GitLab che esegue ad ogni push o merge request il tool [markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2) per il linting dei post Markdown del sito.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/b2f0ebb11a40badfe219f0764581acaefba0a329)

[→ Vedi altro commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/0326720eefb8a0ec27bf9989cdc44218dda2bbc5)

### GitLab CI + compileall

Si è aggiunto un nuovo stage alla pipeline GitLab, lo stage di testing.

Esso esegue il [modulo `compileall`](https://docs.python.org/3/library/compileall.html) di Python, verificando così la validità di sintassi dei file del progetto.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/8ba51ef385b760f105b86ac1f7029164ec4a8f30)

> In questo modo sarà possibile verificare che il codice dell'applicazione sia funzionale prima di procedere con i passi successivi.

### GitLab CI + Docker BuildKit

Si è abilitato il container registry di GitLab dalle impostazioni del progetto:

![](03-containerregistry.png)

> In questo modo si rende disponibile un archivio per distribuire container image da utilizzare per mettere in produzione il sito.

Si è poi aggiunto un'ulteriore stage alla pipeline GitLab, lo stage di building.

Esso esegue [Docker BuildKit](https://docs.docker.com/build/buildkit/) per creare una container image dell'applicazione per ciascun build target definito, e per poi uploadarla al registro immagini di GitLab precedentemente abilitato.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/508ec55e792ec0fd77a93561e44fad967953b338)

> In questo modo le container image dell'applicazione saranno ricreate ad ogni push che passa lo stage di testing, non richiedendo ai manutentori di compilarle e uploadarle manualmente.

### Amazon Web Services

Si è istanziata una macchina virtuale `t2.micro` da [AWS EC2](https://aws.amazon.com/ec2/).

> Per via di considerazioni legate al prezzo, l'istanza è stata configurata come Spot Instance; un vero sito web in produzione dovrebbe evitare questa configurazione in quanto potrebbe essere causa di downtime, e dovrebbe invece utilizzare un'istanza sempre online.

Si è acquisito un indirizzo IPv4 da assegnare all'istanza: `35.180.100.109`

Si è generata una coppia di chiavi SSH:
- la chiave pubblica è stata configurata come autorizzata per la connessione alla macchina virtuale;
- la chiave privata è stata scaricata localmente, e poi caricata nel Secure Files storage di GitLab con il nome di `id_ed25519`.

Si è configurato il gruppo di sicurezza della macchina virtuale per permettere connessioni entranti e uscenti da e verso qualsiasi indirizzo IP e porta.

> In questo modo, l'istanza può essere configurata sia manualmente, sia da un job di GitLab.

Ci si è connessi localmente alla macchina virtuale attraverso SSH per eseguire una configurazione preliminare:

```shell
ssh -i PRIVATE_KEY.pem ec2-user@35.180.100.109
sudo -i
```

Come superutente, si sono installati Docker, Docker Compose, e Git sulla macchina virtuale:

```shell
yum install --assumeyes git
yum install --assumeyes docker
curl -SL https://github.com/docker/compose/releases/download/v2.24.2/docker-compose-linux-x86_64 -o /usr/libexec/docker/cli-plugins/docker-compose
chmod a+x /usr/libexec/docker/cli-plugins/docker-compose
systemctl enable --now docker
```

> In questo modo la macchina virtuale è pronta per l'esecuzione della versione di produzione dell'applicazione.

Si è dato permesso all'utente base `ec2-user` di interagire con il daemon Docker e di accedere alla cartella `/srv/docker`:

```shell
gpasswd --add ec2-user docker
mkdir --parents /srv/docker
chown ec2-user: /srv/docker
```

> In questo modo si potranno in futuro restringere i privilegi posseduti dai job alla sola interazione con il daemon Docker, e non all'alterazione del resto del sistema host.

Infine, si è clonato il repository Git dell'applicazione all'interno della cartella `/srv/docker`:

```shell
git clone https://gitlab.com/Steffo99/cinquantasei.git /srv/docker/cinquantasei
```

### DNS

Si sono configurati manualmente alcuni record DNS sul dominio `steffo.eu`:

```dns
rio                300 IN A     35.180.100.109
cinquantasei       300 IN CNAME rio
stats.cinquantasei 300 IN CNAME rio
```

Si è poi configurato Caddy per usare l'indirizzo <https://cinquantasei.steffo.eu/> per servire l'applicazione in produzione.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/531a87545e152b2928d216e61dde55058f0563f3)

> Avendo un nome di dominio associato, Caddy sarà in grado di richiedere e mantenere automaticamente certificati TLS, in modo da poter servire l'applicazione tramite HTTPS.

### GitLab CI + SSH

Si è creato lo stage finale della pipeline GitLab, lo stage di deployment.

In questo stage, è stato creato un job che:
1. recupera in sicurezza le credenziali SSH dal Secure Files storage di GitLab
2. le usa per connettersi all'istanza EC2 in SSH
3. aggiorna il repository remoto alle ultime modifiche pushate al branch `main`
4. aggiorna o ricompila le container image utilizzate in `compose.yml`
5. ricrea i container definiti in `compose.yml`

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/9439bc6c47894ec470790e4170a9be4a63795e68)

[→ Vedi altro commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/20436d943e349b18f3d4bf25f267dd10b04e178f)

> In questo modo, qualsiasi modifica effettuata al sito che passa i test viene immediatamente riflettuta sul sito in produzione, con downtime minimo.

## Monitoring

### Sentry

Si è configurata l'applicazione per catturare automaticamente le eccezioni incontrate nel rendering delle pagine e inoltrare tutti i dettagli possibili relativi ad esse a un server [Sentry](https://sentry.io/)-compatibile, usando l'integrazione con Flask che l'SDK di Sentry fornisce.

Si è sperimentato con due diversi server Sentry, aggiungendo il loro DSN al Secure Files storage di GitLab come `sentry_dsn.txt`:
- quello integrato di GitLab
- quello ufficiale di Sentry

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/262481be7d3b2d19b835bcebbe542eeda09ddeba)

> In questo modo diventa molto più facile per i manutentori identificare e correggere bug, in quanto spesso gli utenti finiscono per non segnalarli.

#### Server integrato di GitLab

Si sono attivate le funzionalità di monitoring nelle impostazioni del progetto GitLab:

![](04-sentrylab.png)

Si è poi configurato il DSN da utilizzare con quello fornito da GitLab.

[→ Vedi errore catturato di esempio](https://gitlab.com/Steffo99/cinquantasei/-/error_tracking/3395507535/details)

> Si può osservare che le informazioni catturate da GitLab sono molto limitate, e includono solo un conteggio di quante volte l'errore è stato catturato, e una breve stack trace dell'errore.

#### Server ufficiale di Sentry

Si è creato un nuovo progetto su Sentry, e si è configurato il DSN da utilizzare con quello fornito da Sentry.

![](04-sentrysentry.png)

> Si può osservare come le informazioni catturate da Sentry siano molto, molto più dettagliate di quelle catturate da GitLab.

### Prometheus

Si è configurato il reverse proxy Caddy per emettere sulla porta `2019` statistiche relative al suo utilizzo nel formato [OpenTelemetry](https://opentelemetry.io/).

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/b28f95f3f3db2dc7d6915e9925eb9b2968992a5d)

> In questo modo vengono rese disponibili metriche di utilizzo del reverse proxy, che possono essere utili per monitorare il volume di traffico che sta ricevendo il sito e determinare se valga la pena scalarlo verticalmente o eventualmente riscriverlo perchè sia scalabile orizzontalmente.

Si è poi aggiunto allo stack Docker Compose un container [Prometheus](https://prometheus.io/), e lo si è configurato per recuperare e immagazinare periodicamente le metriche emesse da Caddy e da sè stesso.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/5d0ad48cf10859541d0c4fd27acf75f19abc9c4c)

> In questo modo, si ottiene una visuale nel tempo delle metriche, ispezionabili successivamente tramite Grafana.

Si è infine aggiunto un endpoint di metriche all'applicazione stessa, sfruttando la libreria Python [`prometheus_flask_exporter`](https://github.com/rycus86/prometheus_flask_exporter).

Tra le metriche raccolte, si è incluso:

- versione attuale dell'applicazione (che attualmente è impostata a `0.0.0`, in quanto l'applicazione non è packaged)
- un contatore di quante richieste arrivano alla route di home
- un contatore di quante richieste arrivano a ciascuna pagina dell'elenco di post
- un contatore di quante richieste arrivano a ciascun post

Si è poi configurato il Prometheus dello stack per raccogliere anche queste ultime metriche.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/522d197227040eb591ffd57ad12f609d8a2f6063)

### Grafana

Come ultima cosa, si è aggiunto allo stack un container [Grafana](https://grafana.com/), e lo si è reso disponibile a <https://stats.cinquantasei.steffo.eu/>.

[→ Vedi commit associato](https://gitlab.com/Steffo99/cinquantasei/-/commit/494451a4f84fdacc4d835bd803f5c499f8588773)

> In questo modo, diventa possibile visualizzare le metriche raccolte da Prometheus.

Si è quindi creata una dashboard, e la si è popolata con due widget di esempio basati sulle seguenti query PromQL:

```promql
# Richieste al secondo servite da Caddy nell'intervallo desiderato di tempo
rate(caddy_http_requests_total[$__rate_interval])
```
```promql
# Richieste al secondo servite da Flask dirette alla pagina "home"
rate(strangeblog_http_request_home_total[$__rate_interval])
```

![](04-grafana.png)

> In questo modo, è immediatamente visibile il carico di richieste sotto cui si trova il server, ed è possibile intervenire immediatamente se si verifica qualcosa di anormale.

## Miglioramenti effettuabili

Si elencano alcune idee per ulteriori miglioramenti DevOps effettuabili al progetto.

### Editorconfig

Qualora i manutentori desiderassero imporre uno stile di codice uniforme sull'intero progetto, potrebbero decidere di creare un file [`.editorconfig`](https://editorconfig.org/), in modo da configurare automaticamente tutti gli IDE che vi accedono con quello stile.

### Dev Container

Qualora i manutentori desiderassero usare Visual Studio Code come IDE, potrebbe risultare utile configurare un [Dev Container](https://code.visualstudio.com/docs/devcontainers/create-dev-container) per il progetto, rendendo così possibile eseguire l'ambiente di sviluppo da web.

### Kubernetes / Docker Swarm

Qualora il sito web raggiungesse un carico di richiesto tale da necessitare scaling orizzontale, sarà opportuno considerare di ristrutturare l'applicazione per tale situazione, idealmente deployandola attraverso  [Kubernetes](https://kubernetes.io/) o [Docker Swarm](https://docs.docker.com/engine/swarm/).

### OpenTofu

Per istanziare rapidamente l'infrastruttura cloud che l'applicazione dovrà utilizzare, potrebbe essere opportuno creare una configurazione [OpenTofu](https://opentofu.org/) per il progetto.

### Alert di Grafana

È possibile configurare Grafana per inviare automaticamente allarmi a varie destinazioni (come email, Slack, o Telegram) nel caso alcune metriche raggiungano valori anormali, identificando così downtime o sovraccarichi del sito.
