"""Create post_views

Revision ID: ce365f1e48d4
Revises: 
Create Date: 2024-01-25 03:16:58.677562

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ce365f1e48d4'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'post_views',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('post_name', sa.String(), nullable=True),
        sa.Column('views', sa.Integer(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('post_views')
