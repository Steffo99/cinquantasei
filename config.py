import cfig
import sqlalchemy
import sqlalchemy.exc
import sentry_sdk
import pathlib

config = cfig.Configuration()


@config.required()
def SQLALCHEMY_DATABASE_URI(value: str) -> str:
    """
    The URI to be used to access the database.
    See: https://docs.sqlalchemy.org/en/20/core/engines.html#database-urls
    Example: `postgresql://flask:passwordhere@172.17.0.2:5432/blog`
    """
    try:
        sqlalchemy.engine.url.make_url(value)
    except sqlalchemy.exc.ArgumentError:
        raise cfig.InvalidValueError("Parsing failed.")
    return value


@config.optional()
def SENTRY_DSN(value: str | None) -> str | None:
    """
    The DSN to use for Sentry error reporting.
    See: https://docs.sentry.io/platforms/python/integrations/flask/
    See: https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking
    Example: https://asdf_12341234123412341234123412341234@observe.gitlab.com:443/errortracking/api/v1/projects/12341234
    """
    return value


@config.optional()
def SENTRY_SAMPLE_RATE(value: str | None) -> float:
    """
    The chance that an event triggered from Flask will be sent to Sentry.
    Must be adjusted based on the amount of traffic received from the website.
    """
    if value is None:
        return 1.0
    try:
        return float(value)
    except ValueError:
        raise cfig.InvalidValueError("Not a float.")


@config.optional()
def PROMETHEUS_MULTIPROC_DIR(value: str | None) -> pathlib.Path | None:
    """
    If set, forces the application to use the Prometheus metrics exporter with support for multithreading.
    Do not use in development environments; do use with Gunicorn.
    """
    if value is None:
        return None
    path = pathlib.Path(value)
    if not path.is_dir():
        raise cfig.InvalidValueError("Not a directory.")
    return path


if __name__ == "__main__":
    config.cli()
