# Freeze Python version for better forward compatibility
FROM python:3.11 AS base

LABEL org.opencontainers.image.title="Cinquantasei"
LABEL org.opencontainers.image.description="Exercise in employing GitLab CI/CD tooling for the Cloud and Edge Computing exam at Unimore"
LABEL org.opencontainers.image.url="https://gitlab.com/Steffo99/cinquantasei"
LABEL org.opencontainers.image.authors="Stefano Pigozzi <me@steffo.eu>"

WORKDIR /usr/src/strange_blog

COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

COPY . .

# Development mode
FROM base AS dev_app
ENTRYPOINT ["python", "app.py"]
EXPOSE 8080

# Production mode, no reverse proxy
FROM base AS gunicorn_app
ENTRYPOINT ["./entrypoint.sh", "wsgi:app"]
EXPOSE 8080

# Production mode, with reverse proxy
FROM base AS gunicorn_reverse_proxy_app
ENTRYPOINT ["./entrypoint.sh", "wsgi:reverse_proxy_app"]
EXPOSE 8080
