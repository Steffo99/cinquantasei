from prometheus_flask_exporter.multiprocess import GunicornInternalPrometheusMetrics


# noinspection PyUnusedLocal
def child_exit(server, worker):
    # Have gunicorn communicate to prometheus_flask_exporter that a given request worker is not running anymore
    GunicornInternalPrometheusMetrics.mark_process_dead_on_child_exit(worker.pid)
