from werkzeug.middleware.proxy_fix import ProxyFix

# noinspection PyUnresolvedReferences
from app import app

# Parameters tuned for reverse proxying via the Caddy webserver
# Different parameters might be needed for example with Apache or Nginx
reverse_proxy_app = ProxyFix(
    app=app,
    x_for=1,
    x_proto=1,
    x_host=1,
    x_port=0,
    x_prefix=0,
)
