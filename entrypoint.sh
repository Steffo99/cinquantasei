#!/usr/bin/env bash

export FLASK_APP=app.py

flask db upgrade

mkdir ./metrics
export PROMETHEUS_MULTIPROC_DIR=./metrics

# I know what I am doing with $*, IntelliJ!
# I deliberately want to pass all arguments given to this script to gunicorn!
# shellcheck disable=SC2086
# shellcheck disable=SC2048
gunicorn -b 0.0.0.0:8080 $*
